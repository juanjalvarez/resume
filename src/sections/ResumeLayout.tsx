import moment from 'moment'
import React from 'react'
import styled from 'styled-components'

import { Box } from '../elements/Box'
import { ExperienceBox } from '../elements/ExperienceBox'
import { Link } from '../elements/Link'

const Page = styled(Box)`
    padding: 1in;
`

export const ResumeLayout: React.FC = ({ children }) => {
    return (
        <Page column height="100%" backgroundColor="white">
            <Box
                column
                width="100%"
                justifyContent="center"
                alignItems="center"
            >
                <h1>Juan J. Alvarez</h1>
                <div>Full-stack engineer</div>
            </Box>
            <Box width="100%" justifyContent="space-between" marginTop={30}>
                <Box column alignItems="center">
                    <b>Phone</b>
                    <Link href="tel:+17875469168">+1(407)-883-8983</Link>
                </Box>
                <Box column alignItems="center">
                    <b>Email</b>
                    <Link href="mailto:jalvarez5241997@gmail.com">
                        jalvarez5241997@gmail.com
                    </Link>
                </Box>
                <Box column alignItems="center">
                    <b>LinkedIn</b>
                    <Link href="https://www.linkedin.com/in/juanjalvarez97/">
                        juanjalvarez97
                    </Link>
                </Box>
                <Box column alignItems="center">
                    <b>GitHub</b>
                    <Link href="https://github.com/juanjalvarez">
                        juanjalvarez
                    </Link>
                </Box>
            </Box>
            <Box column width="100%" marginTop={30}>
                <h2>Skills</h2>
                <Box width="100%" justifyContent="space-between" marginTop={10}>
                    <Box column>
                        <h3>Key Interests</h3>
                        <span>TypeScript</span>
                        <span>React</span>
                        <span>NodeJS</span>
                        <span>GraphQL</span>
                        <span>Docker</span>
                        <span>Kubernetes</span>
                    </Box>
                    <Box column>
                        <h3>Web</h3>
                        <span>React</span>
                        <span>Next</span>
                        <span>Webpack</span>
                    </Box>
                    <Box column>
                        <h3>Server</h3>
                        <span>Express.js</span>
                        <span>Python + Flask</span>
                        <span>Go</span>
                    </Box>
                    <Box column>
                        <h3>Data</h3>
                        <span>Presto</span>
                        <span>Redis</span>
                        <span>PostgreSQL</span>
                        <span>S3</span>
                    </Box>
                </Box>
            </Box>
            <Box width="100%" column marginTop={30}>
                <Box marginBottom={15}>
                    <h2>Experience</h2>
                </Box>
                <ExperienceBox
                    name="Pinterest"
                    role="Full-stack engineer"
                    start={moment('2018-09-24')}
                    locations={['San Francisco, CA', 'Orlando, FL (Remote)']}
                >
                    At Pinterest I worked on many interesting projects spanning
                    from data visualization tools to internal tooling. Some
                    notable projects that I contributed to were WQuery (Template
                    query tool for fine grained access control), S3Pinadmin (A
                    hierarchical data visualization tool, originally intended
                    for budget/cost analysis of S3 but proved to be great for
                    other use-cases) and ITP (Pinterest's internal tooling
                    platform used across the company). I was also a major
                    contributor to getting my team to adopt TypeScript early
                    into my time at Pinterest.
                </ExperienceBox>
                <br />
                <ExperienceBox
                    name="CareLinx"
                    role="Front-end engineer"
                    start={moment('2017-06-01')}
                    end={moment('2018-09-20')}
                    locations={['San Juan, PR (Remote)']}
                >
                    At CareLinx I did mostly front-end work with some python +
                    django work every now and then. At the time React ES6
                    components were becoming popular and I helped the team
                    migrate. I worked closely with a PM to deliver high value
                    features as the company was still in startup mode. I was one
                    of 5 engineers at the time.
                </ExperienceBox>
                <br />
                <ExperienceBox
                    name="CEGSoft"
                    role="Intern Engineer"
                    start={moment('2016-10-01')}
                    end={moment('2017-03-01')}
                    locations={['San Juan, PR']}
                >
                    CEGSoft was my first job opportunity within the industry, I
                    worked with microsoft technologies primarily and some web
                    technologies.
                </ExperienceBox>
            </Box>
        </Page>
    )
}
