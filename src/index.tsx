import React from 'react'
import ReactDOM from 'react-dom'
import { ResumeLayout } from './sections/ResumeLayout'

ReactDOM.render(<ResumeLayout />, document.getElementById('root'))
