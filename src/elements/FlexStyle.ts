import { css } from 'styled-components'

export type AlignItemValues = 'center' | 'flex-start' | 'flex-end'

export type JustifyContentValues =
    | AlignItemValues
    | 'space-around'
    | 'space-between'
    | 'space-evenly'

export type FlexStyleProps = {
    flex?: number
    inline?: boolean
    column?: boolean
    justifyContent?: JustifyContentValues
    alignItems?: AlignItemValues
}

export const FlexStyle = css<FlexStyleProps>`
    ${({ flex }) => (flex !== undefined ? `flex: ${flex};` : '')}
    display: ${({ inline }) => (inline ? 'inline-flex' : 'flex')};
    flex-direction: ${({ column }) => (column ? 'column' : 'row')};
    justify-content: ${({ justifyContent = 'flex-start' }) => justifyContent};
    align-items: ${({ alignItems = 'flex-start' }) => alignItems};
`
