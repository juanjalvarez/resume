import { css } from 'styled-components'

export type PaddingStyleProps = {
    paddingLeft?: number
    paddingRight?: number
    paddingTop?: number
    paddingBottom?: number
    paddingVertical?: number
    paddingHorizontal?: number
    padding?: number
}

export const PaddingStyle = css<PaddingStyleProps>`
    padding-left: ${({ paddingLeft, paddingHorizontal, padding }) =>
        paddingLeft ?? paddingHorizontal ?? padding ?? 0}px;
    padding-right: ${({ paddingRight, paddingHorizontal, padding }) =>
        paddingRight ?? paddingHorizontal ?? padding ?? 0}px;
    padding-top: ${({ paddingTop, paddingVertical, padding }) =>
        paddingTop ?? paddingVertical ?? padding ?? 0}px;
    padding-bottom: ${({ paddingBottom, paddingVertical, padding }) =>
        paddingBottom ?? paddingVertical ?? padding ?? 0}px;
`
