import styled from 'styled-components'
import { FlexStyle, FlexStyleProps } from './FlexStyle'
import { PaddingStyle, PaddingStyleProps } from './PaddingStyle'
import { MarginStyle, MarginStyleProps } from './MarginStyle'
import { DimensionStyle, DimensionStyleProps } from './DimensionStyle'

export type BoxDisplayValue =
    | 'flex'
    | 'block'
    | 'grid'
    | 'inline-flex'
    | 'inline-block'

export type BoxProps = {
    display?: BoxDisplayValue
    color?: string
    backgroundColor?: string
} & FlexStyleProps &
    DimensionStyleProps &
    MarginStyleProps &
    PaddingStyleProps

export const Box = styled.div<BoxProps>`
    ${({ display = 'flex' }) => (display.includes('flex') ? FlexStyle : '')}
    ${DimensionStyle}
    ${MarginStyle}
    ${PaddingStyle}
    display: ${({ display = 'flex' }) => display};
    color: ${({ color = 'auto' }) => color};
    background-color: ${({ backgroundColor = 'auto' }) => backgroundColor};
`
