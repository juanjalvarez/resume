import { css } from 'styled-components'

export const dimToSize = (val: number | string) => {
    if (typeof val === 'string') {
        return val
    }
    return `${val}px`
}

export type DimensionStyleProps = {
    width?: number | string
    height?: number | string
}

export const DimensionStyle = css<DimensionStyleProps>`
    width: ${({ width }) => (width ? dimToSize(width) : 'auto')};
    height: ${({ height }) => (height ? dimToSize(height) : 'auto')};
`
