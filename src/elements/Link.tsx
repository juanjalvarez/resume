import React from 'react'
import styled, { StyledComponentProps } from 'styled-components'

export const StyledLink = styled.a`
    color: blue;
    text-decoration: underline;
`

export type LinkProps = StyledComponentProps<'a', any, {}, never>

export const Link: React.FC<LinkProps> = ({
    children,
    target,
    ...otherProps
}) => {
    return (
        <StyledLink target="_blank" {...otherProps}>
            {children}
        </StyledLink>
    )
}
