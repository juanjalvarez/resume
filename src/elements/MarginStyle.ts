import { css } from 'styled-components'

export type MarginStyleProps = {
    marginLeft?: number
    marginRight?: number
    marginTop?: number
    marginBottom?: number
    marginVertical?: number
    marginHorizontal?: number
    margin?: number
}

export const MarginStyle = css<MarginStyleProps>`
    margin-left: ${({ marginLeft, marginHorizontal, margin }) =>
        marginLeft ?? marginHorizontal ?? margin ?? 0}px;
    margin-right: ${({ marginRight, marginHorizontal, margin }) =>
        marginRight ?? marginHorizontal ?? margin ?? 0}px;
    margin-top: ${({ marginTop, marginVertical, margin }) =>
        marginTop ?? marginVertical ?? margin ?? 0}px;
    margin-bottom: ${({ marginBottom, marginVertical, margin }) =>
        marginBottom ?? marginVertical ?? margin ?? 0}px;
`
