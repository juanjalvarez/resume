import { faCalendar, faGlobe } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import moment from 'moment'
import React from 'react'
import { Box } from './Box'

type Props = {
    name: string
    role?: string
    start: moment.Moment
    end?: moment.Moment
    locations?: string[]
}

export const ExperienceBox: React.FC<Props> = ({
    name,
    role,
    start,
    end,
    locations,
    children,
}) => {
    const diffMonths = (end ?? moment()).diff(start, 'months')
    const years = Math.floor(diffMonths / 12)
    const months = diffMonths % 12
    const datesString = `${start.format('MMM YYYY')} - ${
        end?.format('MMM YYYY') ?? 'now'
    } (${years > 0 ? `${years}y ` : ''}${months}m)`
    return (
        <Box width="100%" column>
            <Box width="100%" justifyContent="space-between" marginBottom={10}>
                <Box column>
                    <h3>{name}</h3>
                    {role && <h4>{role}</h4>}
                    <Box color="gray" marginTop={5}>
                        <Box marginRight={5}>
                            <FontAwesomeIcon icon={faCalendar} />
                        </Box>
                        {datesString}
                    </Box>
                </Box>
                {locations && (
                    <Box column>
                        {locations.map((loc) => {
                            return (
                                <Box id={loc} color="gray" marginTop={5}>
                                    <Box marginRight={5}>
                                        <FontAwesomeIcon icon={faGlobe} />
                                    </Box>
                                    {loc}
                                </Box>
                            )
                        })}
                    </Box>
                )}
            </Box>
            <Box>{children}</Box>
        </Box>
    )
}
